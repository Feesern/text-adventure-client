import PhrasePreview from './PhrasePreview';
import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
});

class PhraseList extends React.Component {
  	render() {
	  if (!this.props.phrases) {
	    return (
	      <div className="article-preview">Loading...</div>
	    );
	  }

	  if (this.props.phrases.length === 0) {
	    return (
	      <div className="article-preview">
	        No phrasesd... yet.
	      </div>
	    );
	  }

	  return (
	    <div>
	      {
	        this.props.phrases.map(phrase => {
	          return (
	            <PhrasePreview phrase={phrase} key={phrase.id} />
	          );
	        })
	      }
	    </div>
	  );
	}
};

export default connect(mapStateToProps)(PhraseList);