import React from 'react';
import { Button } from 'react-bootstrap';

const LoggedOutView = props => {
  if (!props.currentUser) {
    return (
      <Button href="/#/login" bsStyle="primary">... Login/Register</Button>
    );
  }
  return null;
};

const LoggedInView = props => {
	//If the user is logged in
  	if (props.currentUser && ! props.currentUser.tokens) {
  		//If the user has tokens, show the option to add words, else, show link to get tokens
    	return (
      		<Button href="/#/tokens" bsStyle="primary">... Get Tokens</Button>
    	);
  	}
  return null;
};

const LoggedInWithTokensView = props => {
	//If the user is logged in
  	if (props.currentUser && props.currentUser.tokens) {
  		//If the user has tokens, show the option to add words, else, show link to get tokens
    	return (
      		<Button href="/#/editor" className="disabled" bsStyle="primary">... Add To Story</Button>
    	);
  	}
  return null;
};

class AddWordsLink extends React.Component {
  render() {
    return (
      <div>
      <br />
        <LoggedOutView currentUser={this.props.currentUser} />

        <LoggedInView currentUser={this.props.currentUser} />

        <LoggedInWithTokensView currentUser={this.props.currentUser} />
      </div>
    );
  }
}

export default AddWordsLink;
