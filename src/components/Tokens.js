import React from 'react'
import StripeCheckout from 'react-stripe-checkout';
import agent from '../agent';
import ListSuccess from './ListSuccess';
import { connect } from 'react-redux';
import {
  BUY_TOKENS
} from '../constants/actionTypes';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({ 
  onSubmit: (token, amount) => {
    const payload = agent.Tokens.save(token, amount);
    dispatch({ type: BUY_TOKENS, payload });
  },
});

class Tokens extends React.Component {
  //onToken = (token) => {
  onToken = (amount) => token => {
    this.props.onSubmit(token, amount);
  }

  render() {
    return (
    <div>
      <div className="container">
        <ListSuccess tokens={this.props.common} />
        <div className="row">
          <div className="col-md-offset-1 col-xs-6 col-sm-6 col-md-3 col-lg-3">
            <div className="panel price panel-red">
              <div className="panel-heading  text-center">
              <h3>2 CHEERIO TOKENS</h3>
              </div>
              <div className="panel-body text-center">
                <p className="lead"><strong>$12</strong></p>
              </div>
              <ul className="list-group list-group-flush text-center">
                <li className="list-group-item"><i className="icon-ok text-danger"></i> 2 Tokens</li>
              </ul>
              <div className="panel-footer">
                <StripeCheckout
                  name="Cheerio.co" // the pop-in header title
                  description="Write Something Beautiful" // the pop-in header subtitle
                  ComponentClass="div"
                  panelLabel="Get Tokens" // prepended to the amount in the bottom pay button
                  amount={1200} // cents
                  currency="USD"
                  stripeKey="pk_live_lPBno4Y0Sr5CdHaGxCybxyH1"
                  locale="auto"
                  allowRememberMe // "Remember Me" option (default true)
                  token={this.onToken(1200)} // submit callback
                  opened={this.onOpened} // called when the checkout popin is opened (no IE6/7)
                  closed={this.onClosed} // called when the checkout popin is closed (no IE6/7)
                  >
                  <button className="btn btn-lg btn-block btn-danger">
                    Pay With Card
                  </button>
                </StripeCheckout>
              </div>
            </div>
          </div>
              
          <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            <div className="panel price panel-blue">
              <div className="panel-heading arrow_box text-center">
              <h3>5 CHEERIO TOKENS</h3>
              </div>
              <div className="panel-body text-center">
                <p className="lead"><strong>$29</strong></p>
              </div>
              <ul className="list-group list-group-flush text-center">
                <li className="list-group-item"><i className="icon-ok text-info"></i> 5 Tokens</li>
              </ul>
              <div className="panel-footer">
                <StripeCheckout
                  name="Cheerio.co" // the pop-in header title
                  description="Write Something Beautiful" // the pop-in header subtitle
                  ComponentClass="div"
                  panelLabel="Get Tokens" // prepended to the amount in the bottom pay button
                  amount={2900} // cents
                  currency="USD"
                  stripeKey="pk_live_lPBno4Y0Sr5CdHaGxCybxyH1"
                  locale="auto"
                  allowRememberMe // "Remember Me" option (default true)
                  token={this.onToken(2900)} // submit callback
                  opened={this.onOpened} // called when the checkout popin is opened (no IE6/7)
                  closed={this.onClosed} // called when the checkout popin is closed (no IE6/7)
                  >
                  <button className="btn btn-lg btn-block btn-info">
                    Pay With Card
                  </button>
                </StripeCheckout>
              </div>
            </div>
          </div>

          <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            <div className="panel price panel-white">
              <div className="panel-heading arrow_box text-center">
              <h3>10 CHEERIO TOKENS</h3>
              </div>
              <div className="panel-body text-center">
                <p className="lead"><strong>$58</strong></p>
              </div>
              <ul className="list-group list-group-flush text-center">
                <li className="list-group-item"><i className="icon-ok text-success"></i> 10 Tokens</li>
              </ul>
              <div className="panel-footer">
                <StripeCheckout
                  name="Cheerio.co" // the pop-in header title
                  description="Write Something Beautiful" // the pop-in header subtitle
                  ComponentClass="div"
                  panelLabel="Get Tokens" // prepended to the amount in the bottom pay button
                  amount={5800} // cents
                  currency="USD"
                  stripeKey="pk_live_lPBno4Y0Sr5CdHaGxCybxyH1"
                  locale="auto"
                  allowRememberMe // "Remember Me" option (default true)
                  token={this.onToken(5800)} // submit callback
                  opened={this.onOpened} // called when the checkout popin is opened (no IE6/7)
                  closed={this.onClosed} // called when the checkout popin is closed (no IE6/7)
                  >
                  <button className="btn btn-lg btn-block btn-default">
                    Pay With Card
                  </button>
                </StripeCheckout>
              </div>
            </div>
          </div>
        </div>       
      </div>
    </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tokens);
