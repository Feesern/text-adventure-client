import React from 'react';

class InfoPage extends React.Component {

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="editor-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-10 offset-md-1 col-xs-12">

              <h1>Information</h1>

              <p><a href="/">Cheerio.co</a> is a social writing experiment. Authors will take turns adding 
              their own words to an evolving story. Think of it as a book with many, many authors!</p>
              <p>We hope Cheerio.co grows into something amazing with tons of readers and contributors.</p>
              <p>We want the story to be online forever! But forever is a long time, so we will personally guarantee
              the story will be on <a href="https://cheerio.co">Cheerio.co</a> for at least ten years (barring any
                  regulatory motions -
              <a href="https://www.battleforthenet.com/" target="_blank"> support net neutrality</a>!)</p>

              <h2>Rules</h2>

              <p>We want this to be as fun and open as possible. We only have a few simple rules to follow:</p>

              <ol>
                <li>No advertising in the story</li>
                <li>We reserve the right to change any text we deem inappropriate. We want this to be as fun and open
                    as possible, so let's just keep it clean.</li>
                  <li>No bunching up words to game the system. Youcannotdothis.</li>
              </ol>

              <h2>Roadmap</h2>

              <p>Emoji Support! If we get enough interest, we'll implement support for Emojis. We said we want this to
                  be fun!</p>

              <p>Proper ending. The ending should be great, but deciding when to end an amazing journey can be difficult.</p>

              <p>Voting System. Possibly for the cleanup phase, we will allow users to vote on their favorite word/words to be added to the story.</p>

              <h2>How It Works</h2>

              <p>As a collective group, we have 15 minutes to add a work to the end of the story (including punctuation).</p>
              <p>Users can submit any word they think will go well at the end of the story. Users can also vote for
              words they think will be the best fit. The word with the highest number of votes after 15 minutes will
              be appended to the story and a new 15 minute timer will begin.</p>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InfoPage;
