import React from 'react';
import agent from '../../agent';

const Stats = props => {
  const stats = props.stats;
  if (stats) {
    return (
      <div className="tag-list">
        {
          stats.map(stat => {
            const handleClick = ev => {
              ev.preventDefault();
              props.onClickTag(stat, page => agent.Articles.byTag(stat, page), agent.Articles.byTag(stat));
            };

            return (
              <a
                href=""
                className="tag-default tag-pill"
                key={stat}
                onClick={handleClick}>
                {stat}
              </a>
            );
          })
        }
      </div>
    );
  } else {
    return (
      /*<div>Loading Stats...</div>*/
      <div>
        <p><strong>Word Count:</strong> 1,253</p>
        <p><strong>Tokens Available:</strong>198,833</p>
        <p><strong>Top Contributors:</strong></p>
        <ol>
          <li><a href="#">Mary Smith</a> (233 words)</li>
          <li><a href="#">Lindy Feeser</a> (199 words)</li>
          <li><a href="#">Aaron Gonzalez</a> (176 words)</li>
        </ol>
      </div>
    );
  }
};

export default Stats;
