import React from 'react';
import agent from "../../agent";
import {
    WORD_SUBMITTED
} from "../../constants/actionTypes";
import {connect} from "react-redux";

const mapDispatchToProps = dispatch => ({
    onSubmit: payload =>
        dispatch({ type: WORD_SUBMITTED, payload }),
});

class AddWordInput extends React.Component {
    constructor() {
        super();

        this.submitForm = this.submitForm.bind(this);
    }

    submitForm = ev => {
        ev.preventDefault();

        if (!this.refs.wordInput.value.trim()) {
            return
        }

        const word = {
            word: this.refs.wordInput.value.trim(),
        };

        const promise = agent.Words.create(word);

        this.props.onSubmit(promise);

        this.refs.wordInput.value = ''
    };

    render() {
        if (this.props.currentUser) {
            return (
                <form>
                    <fieldset>

                        <fieldset className="form-group">
                            <input ref="wordInput" className="form-control">
                            </input>
                        </fieldset>

                        <button
                            className="btn btn-lg pull-xs-right btn-primary"
                            type="button"
                            onClick={this.submitForm}>
                            Submit
                        </button>

                    </fieldset>
                </form>
            );
        }
        else { // not logged in
            return (
                <p>You must be logged in to submit words</p>
            );
        }
    }
}

export default connect(null, mapDispatchToProps)(AddWordInput);