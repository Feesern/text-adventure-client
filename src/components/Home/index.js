import Banner from './Banner';
import MainView from './MainView';
import React from 'react';
import NewWordsPanel from './NewWordsPanel';
import agent from '../../agent';
import { connect } from 'react-redux';
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED
} from '../../constants/actionTypes';

const Promise = global.Promise;

//Passes props to the Home component with the connect() call at bottom
const mapStateToProps = state => ({
  ...state.home,
  words: state.word,
  appName: state.common.appName,
  token: state.common.token
});

//passes events to the Home component via connect() below.
const mapDispatchToProps = dispatch => ({
  // onClickTag: (tag, pager, payload) =>
  //   dispatch({ type: APPLY_TAG_FILTER, tag, pager, payload }),
  onLoad: (tab, pager, payload) =>
    dispatch({ type: HOME_PAGE_LOADED, tab, pager, payload }),
  onUnload: () =>
    dispatch({  type: HOME_PAGE_UNLOADED })
});

class Home extends React.Component {
  componentWillMount() {
    const tab = 'all';

    const wordsPromise = agent.Words.all;

    this.props.onLoad(tab, wordsPromise, Promise.all([wordsPromise()]));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">

        <Banner token={this.props.token} appName={this.props.appName} />

        <div className="container page">
          <div className="row">
            <MainView />

            <div className="col-md-3">
              <div className="sidebar">

                <h3>Commands</h3>

                <NewWordsPanel
                  words={this.props.words} />

              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
