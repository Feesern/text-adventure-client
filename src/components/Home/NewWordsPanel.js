import React from 'react';
import AddWordInput from './AddWordInput';
import {connect} from "react-redux";

const mapStateToProps = state => ({
    currentUser: state.common.currentUser,
});

class NewWordsPanel extends React.Component {
    render() {
        if (!this.props.words.words) {
            return (
                <div>
                    <p>Loading here...</p>
                    <AddWordInput currentUser={this.props.currentUser} />
                </div>
            );
        }

        if (this.props.words.words.length === 0) {
            return (
                <div className="article-preview">
                    No words yet
                </div>
            );
        }

        return (
            <div>
                <ol>
                    {
                        this.props.words.words.map(word => {
                            return (
                                <li key={word.id}>
                                    {word.word}</li>
                            );
                        })
                    }
                </ol>
                <AddWordInput currentUser={this.props.currentUser}/>
            </div>
        );
    }
}

export default connect(mapStateToProps)(NewWordsPanel);