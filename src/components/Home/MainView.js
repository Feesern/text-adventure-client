import PhraseList from '../PhraseList';
import React from 'react';
import AddWordsLink from '../AddWordsLink';
import { connect } from 'react-redux';
import { CHANGE_TAB } from '../../constants/actionTypes';

const YourFeedTab = props => {
  if (props.token) {
    const clickHandler = ev => {
      ev.preventDefault();
    }

    return (
      <li className="nav-item">
        <a  href=""
            className={ props.tab === 'feed' ? 'nav-link active' : 'nav-link' }
            onClick={clickHandler}>
          The Story
        </a>
      </li>
    );
  }
  return null;
};

const mapStateToProps = state => ({
  ...state.phraseList,
  token: state.common.token,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  onTabClick: (tab, pager, payload) => dispatch({ type: CHANGE_TAB, tab, pager, payload })
});

const MainView = props => {
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">

          <YourFeedTab
            token={props.token}
            tab={props.tab} />

        </ul>
      </div>

      <PhraseList
        phrases={props.phrases}
        loading={props.loading} />

      <AddWordsLink
         currentUser={props.currentUser} />
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
