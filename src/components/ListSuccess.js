import React from 'react';

class ListSuccess extends React.Component {
  render() {
    const tokens = this.props.tokens;
    console.log(this.props);
    if (tokens.user) {
      return (
        <ul className="success-messages">
                <li>
                  Successfully purchased tokens. Total tokens: {tokens.user.tokens}
                </li>
        </ul>
      );
    } else {
      return null;
    }
  }
}

export default ListSuccess;
