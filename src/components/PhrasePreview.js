import React from 'react';
//import agent from '../agent';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  // favorite: slug => dispatch({
  //   type: ARTICLE_FAVORITED,
  //   payload: agent.Articles.favorite(slug)
  // }),
  // unfavorite: slug => dispatch({
  //   type: ARTICLE_UNFAVORITED,
  //   payload: agent.Articles.unfavorite(slug)
  // })
});

const PhrasePreview = props => {
  const phrase = props.phrase;

  return (
        <span className="phrase-text">{phrase.phrase} </span>
  );
}

export default connect(() => ({}), mapDispatchToProps)(PhrasePreview);
