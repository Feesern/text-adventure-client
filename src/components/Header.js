import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
	...state,
 });

const LoggedOutView = props => {
  if (!props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="login" className="nav-link">
            Sign In
          </Link>
        </li>

        <li className="nav-item">
          <Link to="register" className="nav-link">
            Register
          </Link>
        </li>

        <li className="nav-item">
          <Link to="info" className="nav-link">
            <i className="ion-ios-list-outline"></i>&nbsp;Info
          </Link>
        </li>

      </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="info" className="nav-link">
            <i className="ion-ios-list-outline"></i>&nbsp;Info
          </Link>
        </li>

        <li className="nav-item">
          <Link
            to={`settings`}
            className="nav-link">
            {/*<img src={props.currentUser.image} className="user-pic" alt={props.currentUser.username} />*/}
            {props.currentUser.username} ({props.currentUser.tokens || 0} Tokens)
          </Link>
        </li>

      </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-light">
        <div className="container">

          <Link to="/" className="navbar-brand">
            {this.props.appName.toLowerCase()}
          </Link>

          <LoggedOutView currentUser={this.props.currentUser} />

          <LoggedInView currentUser={this.props.currentUser} />
        </div>
      </nav>
    );
  }
}

export default connect(mapStateToProps)(Header);
