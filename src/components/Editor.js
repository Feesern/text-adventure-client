import ListErrors from './ListErrors';
import PhraseList from './PhraseList';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  ADD_TAG,
  EDITOR_PAGE_LOADED,
  REMOVE_TAG,
  ARTICLE_SUBMITTED,
  EDITOR_PAGE_UNLOADED,
  UPDATE_FIELD_EDITOR
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.phraseList,
  ...state.editor,
});

const mapDispatchToProps = dispatch => ({
  onAddTag: () =>
    dispatch({ type: ADD_TAG }),
  // onLoad: payload =>
  //   dispatch({ type: EDITOR_PAGE_LOADED, payload }),
  onLoad: (tab, pager, payload) =>
    dispatch({ type: EDITOR_PAGE_LOADED, tab, pager, payload }),
  onRemoveTag: tag =>
    dispatch({ type: REMOVE_TAG, tag }),
  onSubmit: payload =>
    dispatch({ type: ARTICLE_SUBMITTED, payload }),
  onUnload: payload =>
    dispatch({ type: EDITOR_PAGE_UNLOADED }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_EDITOR, key, value })
});

class Editor extends React.Component {
  constructor() {
    super();

    this.state = {
      words_used: 0
    };

    const updateFieldEvent =
      key => ev => this.props.onUpdateField(key, ev.target.value);
    this.changeBody = updateFieldEvent('body');

    this.submitForm = ev => {
      ev.preventDefault();

      const phrase = {
        phrase: this.props.body,
      };

      console.log("phrase: ");
      console.log(phrase);

      const promise = agent.Phrases.create(phrase);

      this.props.onSubmit(promise);
    };
  }

  componentWillReceiveProps(nextProps) {
    // if (this.props.params.slug !== nextProps.params.slug) {
    //   if (nextProps.params.slug) {
    //     this.props.onUnload();
    //     return this.props.onLoad(agent.Phrases.get(this.props.params.slug));
    //   }
    //   this.props.onLoad(null);
    // }
  }

  componentWillMount() {
    const tab = 'all';

    const phrasesPromise = agent.Phrases.all;

    this.props.onLoad(tab, phrasesPromise, Promise.all([phrasesPromise()]));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  getWordCount(words) {
  	return words.trim().split(' ').length ? words.trim().split(' ').length : 0;
  }

  handleChange(event) {
	var input = event.target.value;
	this.setState({
	    words_used: this.getWordCount(input)
	});

	this.changeBody(event);
  }

  render() {
    return (
      <div className="editor-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-10 offset-md-1 col-xs-12">

              <ListErrors errors={this.props.errors}></ListErrors>

              <p>Note: A space will be added before and after your phrase automatically.</p>

              <form>
                <fieldset>

                  <fieldset className="form-group">
                    <textarea
                      className="form-control"
                      rows="8"
                      placeholder="Compose your phrase... wisely?"
                      value={this.props.body}
                      onChange={this.handleChange.bind(this)}>
                    </textarea>
                  </fieldset>
                  <p>Word Count: {this.state.words_used}</p>

                  <button
                    className="btn btn-lg pull-xs-right btn-primary"
                    type="button"
                    disabled={this.props.inProgress}
                    onClick={this.submitForm}>
                    Submit Words
                  </button>

                </fieldset>
              </form>

            </div>
          </div>

          <br />
          <h2>Current Story</h2>
          <PhraseList
            phrases={this.props.phrases}
            loading={this.props.loading} />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
