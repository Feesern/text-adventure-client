import phrase from './reducers/phrase';
import phraseList from './reducers/phraseList';
import auth from './reducers/auth';
import { combineReducers } from 'redux';
import common from './reducers/common';
import editor from './reducers/editor';
import home from './reducers/home';
import profile from './reducers/profile';
import settings from './reducers/settings';
import word from './reducers/word';
import tokens from './reducers/tokens';

export default combineReducers({
  phrase,
  phraseList,
  auth,
  common,
  editor,
  home,
  profile,
  settings,
  word,
  tokens,
});
