import {
  ARTICLE_FAVORITED,
  ARTICLE_UNFAVORITED,
  SET_PAGE,
  APPLY_TAG_FILTER,
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  EDITOR_PAGE_LOADED,
  EDITOR_PAGE_UNLOADED,
  CHANGE_TAB,
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
  PROFILE_FAVORITES_PAGE_LOADED,
  PROFILE_FAVORITES_PAGE_UNLOADED
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case ARTICLE_FAVORITED:
    case ARTICLE_UNFAVORITED:
      return {
        ...state,
        phrases: state.articles.map(phrase => {
          if (phrase.slug === action.payload.phrase.slug) {
            return {
              ...phrase,
              favorited: action.payload.phrase.favorited,
              favoritesCount: action.payload.phrase.favoritesCount
            };
          }
          return phrase;
        })
      };
    case SET_PAGE:
      return {
        ...state,
        phrases: action.payload.phrases,
        phrasesCount: action.payload.phrasesCount,
        currentPage: action.page
      };
    case APPLY_TAG_FILTER:
      return {
        ...state,
        pager: action.pager,
        phrases: action.payload.phrases,
        phrasesCount: action.payload.phrasesCount,
        tab: null,
        tag: action.tag,
        currentPage: 0
      };
    case HOME_PAGE_LOADED:
      //console.log(action.payload);
      return {
        ...state,
        //pager: action.pager,
        //tags: action.payload[0].tags,
        phrases: action.payload[0].phrases,
        //phrasesCount: action.payload[1].count,
        currentPage: 0,
        tab: action.tab
      };
    case HOME_PAGE_UNLOADED:
      return {};
    case EDITOR_PAGE_LOADED:
      //console.log(action.payload);
      return {
        ...state,
        //pager: action.pager,
        //tags: action.payload[0].tags,
        phrases: action.payload[0].phrases,
        //phrasesCount: action.payload[1].count,
        currentPage: 0,
        tab: action.tab
      };
    case EDITOR_PAGE_UNLOADED:
      return {};
    case CHANGE_TAB:
      return {
        ...state,
        pager: action.pager,
        phrases: action.payload.phrases,
        phrasesCount: action.payload.phrasesCount,
        tab: action.tab,
        currentPage: 0,
        tag: null
      };
    case PROFILE_PAGE_LOADED:
    case PROFILE_FAVORITES_PAGE_LOADED:
      return {
        ...state,
        pager: action.pager,
        phrases: action.payload[1].phrases,
        phrasesCount: action.payload[1].phrasesCount,
        currentPage: 0
      };
    case PROFILE_PAGE_UNLOADED:
    case PROFILE_FAVORITES_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
