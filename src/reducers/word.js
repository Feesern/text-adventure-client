import {
    HOME_PAGE_LOADED,
    WORD_SUBMITTED
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
      case WORD_SUBMITTED:
          return {
              ...state,
              words: action.payload.words,
              errors: action.error ? action.payload.errors : null
          };
      case HOME_PAGE_LOADED:
          return {
              ...state,
              words: action.payload[0].words,
          };
    default:
      return state;
  }
};
